       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMICAL.
       AUTHOR. Mankhong.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT   PIC 999.
       01  HEIGHT   PIC 999.
       01  BMI      PIC 999V9.

       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "Input your Weight : "  WITH NO ADVANCING 
           ACCEPT WEIGHT 
           DISPLAY  "Input your Height : " WITH NO ADVANCING 
           ACCEPT HEIGHT 
           COMPUTE BMI = WEIGHT / ((HEIGHT /100)**2)
           DISPLAY "BMI = "BMI
           IF BMI < 18.5 THEN
              DISPLAY "UNDERWEIGHT"
           ELSE 
              IF BMI >= 18.5 AND BMI <= 24.9 THEN
                 DISPLAY "NORMAL"
              END-IF 
              IF BMI >= 25  AND <= 29.9 THEN
                 DISPLAY "OVERWEIGHT"
              END-IF 
              IF BMI >= 30 AND <= 34.9 THEN
                 DISPLAY "OBESE"
              END-IF 
              IF BMI >= 35  THEN
                 DISPLAY "EXTREMLY OBESE"
              END-IF
           END-IF 
           
           .